import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from './authorization/sign-in.component';
import {DiningComponent} from './common/dining.component';
import {FriendsComponent} from './common/friends.component';
import {PlacesComponent} from './common/places.component';
import {BookComponent} from './common/book/book.component';
import {DictatorComponent} from './common/dictator.component';

export const routes: Routes = [
    {path: '', redirectTo: 'places', pathMatch: 'full'},
    {path: 'places', component: PlacesComponent},
    {path: 'friends', component: FriendsComponent},
    {path: 'dines', component: DiningComponent},
    {path: 'login', component: SignInComponent},
    {path: 'book/:id', component: BookComponent},
    {path: 'dictators', component: DictatorComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class RoutingModule {

}
