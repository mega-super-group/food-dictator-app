import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SignInComponent} from './authorization/sign-in.component';
import {AuthAcceptComponent} from './authorization/auth.accept.component';
import {LogoutComponent} from './authorization/logout.component';
import {PlacesComponent} from './common/places.component';
import {DiningComponent} from './common/dining.component';
import {FriendsComponent} from './common/friends.component';
import {RoutingModule} from './routing.module';
import {AuthServiceConfig, FacebookLoginProvider, SocialLoginModule} from 'angular-6-social-login';
import {ApiService} from './service/api.service';
import {HttpClientModule} from '@angular/common/http';
import {PlaceComponent} from './common/entities/place.component';
import {BookComponent} from './common/book/book.component';
import {FriendComponent} from './common/entities/friend.component';
import {DictatorComponent} from './common/dictator.component';

// Configs
export function getAuthServiceConfigs() {
    const config = new AuthServiceConfig(
        [
            {
                id: FacebookLoginProvider.PROVIDER_ID,
                provider: new FacebookLoginProvider("922697731248763")
            }
        ]
    );
    return config;
}

@NgModule({
  declarations: [
    AppComponent, SignInComponent, AuthAcceptComponent,
      LogoutComponent, PlacesComponent, DiningComponent, FriendsComponent,
      PlaceComponent, BookComponent, FriendComponent, DictatorComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, CommonModule, FormsModule, RoutingModule, SocialLoginModule
  ],
  providers: [ApiService, {
    provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
