import {Observable} from 'ts-observables';
import {User} from './service/api.service';

export class App {
    
    private static currentUser: User = null;
    
    private static authorizationObserver = new Observable();
    
    private static authorized = false;
    
    public static init() {
        
        this.authorizationObserver.appendActionType("authorization");
        this.authorizationObserver.appendActionType("logout");
        
    }
    
    public static onAuthorization(fn: (observation) => void): number {
        
        return this.authorizationObserver.on("authorization", fn);
        
    }
    
    public static onLogoutEvent(fn: (observation) => void): number {
        
        return this.authorizationObserver.on("logout", fn);
        
    }
    
    public static unsubscribeEvent(id: number) {
        
        this.authorizationObserver.unsubscribe(id);
        
    }
    
    public static setAuthorized() {
    
        this.authorized = true;
        
        this.authorizationObserver.pushActionUpdate("authorization", true);
        
    }
    
    public static setUnauthorized() {
        
        this.authorized = false;
        
        this.currentUser = null;
        
    }
    
    public static getUser(): User {
        
        return this.currentUser;
        
    }
    
    public static setUser(user: User) {
        
        this.currentUser = user;
        
        this.storeKeySet("auth_user", user);
        
    }
    
    public static tryLoadUser(): boolean {
        
        const user = this.restoreKeySet<User>("auth_user");
        
        if(user !== null && typeof user === "object") {
            
            if(typeof user.accessKeys === "undefined") {
                this.removeKeySet("auth_user");
                return false;
            }
            
            const key = user.accessKeys[0];
            
            if(typeof key !== "undefined") {
                
                const time = Date.now();
                
                if(key.expiresAt < time) {
                    
                    this.currentUser = user;
                    return true;
                    
                }
            
            }
            
        }
        
        return false;
        
    }
    
    public static isAuthorized() {
        
        return this.authorized;
        
    }
    
    private static storeKeySet(key: string, data: any) {
        
        window.localStorage.setItem(key, JSON.stringify(data));
        
    }
    
    private static restoreKeySet<T>(key: string) {
    
        return JSON.parse(window.localStorage.getItem(key)) as T;
        
    }
    
    private static removeKeySet(key: string) {
        
        return window.localStorage.removeItem(key);
        
    }
    
}

export interface PersistentKeySet {
    accessToken: string;
}
