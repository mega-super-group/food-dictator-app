import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Place} from '../../service/api.service';

@Component({
    selector: 'app-place',
    template: `
        
        <div class="container">

            <div class="row">
                
                <div class="col-md-12">
                    
                    <div class="card">

	                    <div class="row">

		                    <div class="col-md-12 text-left">
                                <h3>{{place?.name}}</h3>
                            </div>
                            
                        </div>

	                    <div class="row">

		                    <div class="col-md-12">
			                    Rating: {{place?.rating}}
		                    </div>

	                    </div>

	                    <div class="row">

		                    <div class="col-md-12">
			                    Visits: {{place?.visits}}
		                    </div>

	                    </div>

	                    <div class="row">

		                    <div class="col-md-8">
                                <small>{{place?.address}}</small>
		                    </div>

		                    <div class="col-md-4 text-right">
			                    <button *ngIf="bookable" class="btn btn-primary" (click)="clickBookEvent()">Book</button>
		                    </div>

	                    </div>
                     
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
  `,
    styles: [`
  
  `]
})

export class PlaceComponent implements OnInit {
    
    @Input()
    public bookable = true;
    
    @Input()
    public place: Place = null;
    
    @Output()
    public bookEvent: EventEmitter<Place> = new EventEmitter();
    
    constructor() {
    
    }
    
    public clickBookEvent() {
        this.bookEvent.emit(this.place);
    }
    
    ngOnInit(): void {
    
    }
    
}
