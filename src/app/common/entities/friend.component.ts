import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../service/api.service';

@Component({
    selector: 'app-friend',
    template: `
        
        <div class="container">

            <div class="row">
                
                <div class="col-md-12">
                    
                    <div class="card">

	                    <div class="row">

		                    <div class="col-md-12 text-left">
                                <h3>{{friend?.name}}</h3>
                            </div>
                            
                        </div>

	                    <div class="row">

		                    <div class="col-md-12">
			                    {{friend?.email}}
		                    </div>

	                    </div>

	                    <div class="row">

		                    <div class="col-md-8">
                                <small>Dines: {{friend?.dinesBeen}}</small>
		                    </div>

	                    </div>
                     
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
  `,
    styles: [`
  
  `]
})

export class FriendComponent implements OnInit {
    
    @Input()
    public friend: User = null;
    
    constructor() {
    
    }
    
    ngOnInit(): void {
    
    }
    
}
