import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ApiService, User} from '../service/api.service';
import {App} from '../app';

@Component({
    selector: 'app-friends',
    template: `
        
        <div class="container">

            <div class="row" *ngFor="let f of friends">
                
                <div class="col-md-12">
                    
                    <app-friend [friend]="f" (click)="selectFriend(f)"></app-friend>
                    
                </div>
                
            </div>
            
        </div>
        
  `,
    styles: [`
  
  `]
})

export class FriendsComponent implements OnInit {

    public friends: User[] = [];
    
    @Output()
    public friendSelected: EventEmitter<User> = new EventEmitter();
    
    constructor(readonly api: ApiService) {
    
    }
    
    public retrieveFriends() {
        
        this.api.myFriends(App.getUser().accessKeys[0]).subscribe(friends => {
            
            this.friends = friends;
            
        });
        
    }
    
    public selectFriend(friend: User) {
        this.friendSelected.emit(friend);
    }
    
    ngOnInit(): void {
        
        if(App.isAuthorized()) {
            
            this.retrieveFriends();
            
        }
        
        App.onAuthorization(() => {
            
            this.retrieveFriends();
            
        });
        
    }
    
}
