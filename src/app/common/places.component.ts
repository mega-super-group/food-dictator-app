import {Component, OnInit} from '@angular/core';
import {ApiService, Place} from '../service/api.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-places',
    template: `
        
        <div class="container">

            <div class="row">
                
                <div class="col-md-3"></div>
	            <div class="col-md-6">
              
		            <h1 class="text-center"> Places </h1>
                    
                    <div class="row">
                        
                        <div class="col-md-12">

	                        <div class="form-group">
		                        <label for="foodtype">Food type</label>
		                        <input type="text"
                                       class="form-control"
                                       id="foodtype"
                                       aria-describedby="foodtyoe"
                                       placeholder="Put few food types"
                                       [(ngModel)]="foodType">
		                        <small id="emailHelp" class="form-text text-muted">
                                    Best food type is always good for your stomach
                                </small>
	                        </div>
                         
	                        <div class="dropdown text-center">
		                        <button class="btn btn-secondary dropdown-toggle"
                                        type="button"
                                        id="dropdownMenuButton"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">
			                        {{currentSearchType}}
		                        </button>
                                
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	                                <div class="dropdown-item"
                                         *ngFor="let st of searchTypes"
                                         (click)="changeRequestType(st)">{{st}}
                                    </div>
                                </ul>
                                
	                        </div>
                        </div>
                        
                    </div>
                    
                    <span *ngIf="loading">
                        One moment please, loading places for you
                    </span>

		            <div class="row" *ngFor="let place of places">

			            <div class="col-md-12">
                            <app-place [place]="place" (bookEvent)="onBookEvent($event)"></app-place>
                        </div>
                        
                    </div>
                    
                </div>
	            <div class="col-md-3"></div>
             
            </div>
            
            
        </div>
        
  `,
    styles: [`
  
  `]
})

export class PlacesComponent implements OnInit {

    public places: Place[] = [];
    
    public loading = true;
    
    public currentSearchType = "Restaurant";
    
    public foodType = "";
    
    public searchTypes: string[] = [
        "Restaurant",
        "Brunch",
        "Cafe",
        "Dinner",
        "Bar",
        "Lunch"
    ];
    
    public errorMessage: string = null;
    
    constructor(readonly apiService: ApiService, readonly router: Router) {
    
    }
    
    public getCoordinates() {
        
        navigator.geolocation.getCurrentPosition(
            (args) => {this.currentPositionResolver(args);},
            (args)=> {this.cannotResolvePosition(args);});
        
    }
    
    public onBookEvent(ev: Place) {
        
        this.router.navigate(["book/"+ev.id]);
        
    }
    
    public currentPositionResolver(position: any) {
        
        const lat = Number(position.coords.latitude);
        const lon = Number(position.coords.longitude);
        
        const type = this.foodType.length === 0 ? "food" : this.foodType;
        
        this.apiService.placesAround(type, lat, lon, this.currentSearchType).subscribe(places => {
            this.places = places;
            this.loading = false;
        });
        
    }
    
    public changeRequestType(type: string) {
    
        this.currentSearchType = type;
        this.loading = true;
        this.getCoordinates();
    
    }
    
    public cannotResolvePosition(error: any) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                this.errorMessage = "User denied the request for Geolocation.";
                break;
            case error.POSITION_UNAVAILABLE:
                this.errorMessage = "Location information is unavailable.";
                break;
            case error.TIMEOUT:
                this.errorMessage = "The request to get user location timed out.";
                break;
            case error.UNKNOWN_ERROR:
                this.errorMessage = "An unknown error occurred.";
                break;
        }
    }
    
    ngOnInit(): void {
        this.getCoordinates();
    }
    
}
