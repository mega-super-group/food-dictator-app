import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ApiService, Dine, Place, User} from '../../service/api.service';
import {ActivatedRoute} from '@angular/router';
import {App} from '../../app';

@Component({
    selector: 'app-book',
    template: `
        
        <div class="container">

	        <div class="row">

		        <div class="col-md-6">
                
                    <h3 *ngIf="dine !== null"> Reservation #{{dine?.id}} created</h3>
                    
                </div>
                
            </div>
            
            <div class="row">
                
                <div class="col-md-6">
                    
                    <h2> Book place </h2>

	                <div class="form-group">
		                <label for="title">Title</label>
		                <input type="text"
                               class="form-control"
                               id="title"
                               aria-describedby="textHelp"
                               placeholder="Title" [(ngModel)]="title">
		                <small id="textHelp" class="form-text text-muted">Describe title of event</small>
	                </div>

	                <div class="form-group">
		                <label for="message">Message</label>
		                <input type="text"
                               class="form-control"
                               id="message"
                               aria-describedby="msgHelp"
                               placeholder="Message" [(ngModel)]="message">
		                <small id="msgHelp" class="form-text text-muted">Write some warm message to your friends.</small>
	                </div>
                 
	                <div *ngIf="place === null">
		                Loading information...
	                </div>
                    
                    <div *ngIf="place !== null">
                        <app-place [place]="place" [bookable]="false"></app-place>
                    </div>
                    
                    <h3>Invited</h3>
                    <div class="row" *ngFor="let f of friends">
                        <div class="col-md-12">
                            {{f.name}}
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn-primary btn" (click)="clickBookEvent()"> Create Dine reservation </button>
                        </div>
                    </div>
                    
                </div>

	            <div class="col-md-6">

		            <h2> Invite friends </h2>

		            <app-friends (friendSelected)="onFriendSelection($event)"></app-friends>

	            </div>
                
            </div>
            
        </div>
        
  `,
    styles: [`
  
  `]
})

export class BookComponent implements OnInit, OnDestroy {
    
    public sub: any = null;
    
    public set id(id: number) {
        this.pullPlaceInformation(id);
    }
    
    public title = "";
    public message = "";
    
    @Input()
    public place: Place = null;
    
    @Input()
    public dine: Dine = null;
    
    public friends: User[] = [];
    
    @Output()
    public bookEvent: EventEmitter<Dine> = new EventEmitter();
    
    constructor(private route: ActivatedRoute, readonly api: ApiService) {
    
    }
    
    public pullPlaceInformation(id: number) {
        
        this.api.place(id).subscribe(place => {
            this.place = place;
        });
        
    }
    
    public onFriendSelection(ev: User) {
        
        const presence = this.friends.filter(f => ev.id === f.id);
        
        if(presence.length > 0) {
            this.friends = this.friends.filter(f => ev.id !== f.id);
        } else {
            this.friends.push(ev);
        }
        
    }
    
    public clickBookEvent() {
        
        this.api.createDine(App.getUser().accessKeys[0], this.title, this.message, this.friends, this.place).subscribe(dine => {
           
            this.dine = dine;
            this.bookEvent.emit(this.dine);
            
        });
        
    }
    
    ngOnInit(): void {
        this.sub = this.route.params.subscribe(params => {
            this.id = Number(params['id']);
        });
    }
    
    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    
}
