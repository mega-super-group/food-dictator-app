import {Component, OnInit} from '@angular/core';
import {ApiService, Place, User} from '../service/api.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-dictators',
    template: `
        
        <div class="container">

            <div class="row">
                
                <div class="col-md-3"></div>
	            <div class="col-md-6">
              
		            <div class="row" *ngFor="let d of dictators">

			            <div class="col-md-8 text-left">
                            {{d?.name}}
                        </div>

			            <div class="col-md-4 text-right">
				            score: {{d?.dinesGathered}}
			            </div>
                    
                    </div>
                
                </div>
	            <div class="col-md-3"></div>
             
            </div>
            
            
        </div>
        
  `,
    styles: [`
  
  `]
})

export class DictatorComponent implements OnInit {
    
    public dictators: User[] = [];
    
    public loading = true;
    
    public errorMessage: string = null;
    
    constructor(readonly apiService: ApiService, readonly router: Router) {
    
    }
    
    public fetch() {
        
        this.apiService.getDictators().subscribe(d => {
            
            this.dictators = d;
            
        });
        
    }
    
    ngOnInit(): void {
        this.fetch();
    }
    
}
