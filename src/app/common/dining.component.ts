import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ApiService, Dine, User} from '../service/api.service';
import {App} from '../app';

@Component({
    selector: 'app-dining',
    template: `
        
        <div class="container">

            <h2> Dining component </h2>
            
            <div class="row">
            
                <div class="col-md-6">

	                <div class="row" *ngFor="let dine of activeDines">

		                <div class="col-md-12">

                            <h3> Dine #{{dine.id}} </h3>
                            <h5> {{dine.title}}</h5>
                            <div>
                                <small>{{dine.message}}</small>
                            </div>
                            <button class="btn btn-primary" (click)="finishDine(dine)"> Finalize </button>

		                </div>
                    
                    </div>
                    
                </div>

	            <div class="col-md-6">

		            <div class="row" *ngFor="let dine of pastDines">

			            <div class="col-md-12">

				            <h3> Dine #{{dine.id}} </h3>
				            <h5> {{dine.title}}</h5>
				            <div>
					            <small>{{dine.message}}</small>
				            </div>

			            </div>

		            </div>
              
	            </div>
            
            </div>
            
        </div>
        
  `,
    styles: [`
  
  `]
})

export class DiningComponent implements OnInit {
    
    public pastDines: Dine[] = [];
    public activeDines: Dine[] = [];
    
    @Output()
    public friendSelected: EventEmitter<User> = new EventEmitter();
    
    constructor(readonly api: ApiService) {
    
    }
    
    public retrieveDines() {
        
        this.api.myPastDines(App.getUser().accessKeys[0]).subscribe(dines => {
            
            this.pastDines = dines;
            
        });
    
        this.api.myActiveDines(App.getUser().accessKeys[0]).subscribe(dines => {
        
            this.activeDines = dines;
        
        });
        
    }
    
    public finishDine(dine: Dine) {
    
        this.api.finalizeDine(App.getUser().accessKeys[0], dine.id).subscribe(d => {
           
            this.retrieveDines();
            
        });
    
    }
    
    ngOnInit(): void {
        
        if(App.isAuthorized()) {
            
            this.retrieveDines();
            
        }
        
        App.onAuthorization(() => {
    
            this.retrieveDines();
            
        });
        
    }

}
