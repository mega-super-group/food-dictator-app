import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {
    
    readonly target: string = null;
    
    constructor(private client: HttpClient) {
        this.target = environment.host;
    }
    
    /**
     * Create or get user for some key authorized by Facebook
     * -----------------------------------------------------------------------
     * @param key
     */
    public checkAuthorization(key: AccessKey): Observable<void> {
        
        return this.client.get<void>(this.target + "/api/v1/authorized", {
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Create or get user for some key authorized by Facebook
     * -----------------------------------------------------------------------
     * @param key
     */
    public createOrGetUserUsingFacebookKey(key: string): Observable<User> {
    
        return this.client.post<User>(this.target + "/api/v1/user/fid/" + key, {});
        
    }
    
    /**
     * Create dine with set of invitations
     * -----------------------------------------------------------------------
     * @param key
     * @param title
     * @param message
     * @param guests
     * @param place
     */
    public createDine(key: AccessKey,
                      title: string,
                      message: string,
                      guests: User[],
                      place: Place): Observable<Dine> {
        
        const body: Dine = {
            title,
            message,
            guests,
            place
        };
    
        return this.client.post<Dine>(
            this.target + "/api/v1/dine",
            body,
            {
                headers: {
                    "Authorization":"Bearer " + key.key
                }
            });
        
    }
    
    /**
     * Get list of places for some lat & lon
     * -----------------------------------------------------------------------
     * @param keyword
     * @param lat
     * @param lon
     * @param type
     */
    public placesAround(keyword: string, lat: number, lon: number, type: string): Observable<Place[]> {
    
        const url = this.target + "/api/v1/places/search/location?lat=" + lat + "&lon=" + lon +"&type=" + type + "&keyword="+keyword;
        
        return this.client.get<Place[]>(url);
        
    }
    
    /**
     * Get list of places stored on system
     * -----------------------------------------------------------------------
     * @param key
     */
    public placesList(key: string) {
    
        const url = this.target + "/api/v1/places";
    
        return this.client.get<Place[]>(url);
        
    }
    
    /**
     * Get particular place
     * -----------------------------------------------------------------------
     * @param id
     */
    public place(id: number) {
    
        const url = this.target + "/api/v1/place/" + id;
    
        return this.client.get<Place>(url);
        
    }
    
    /**
     * Finalize some dine ID
     * -----------------------------------------------------------------------
     * @param key
     * @param id
     */
    public finalizeDine(key: AccessKey, id: number) {
    
        const url = this.target + "/api/v1/dine/" + id + "/close";
    
        return this.client.put<Dine>(url, {}, {
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Get list of active dines
     * -----------------------------------------------------------------------
     * @param key
     */
    public myActiveDines(key: AccessKey) {
    
        const url = this.target + "/api/v1/me/dines/active";
    
        return this.client.get<Dine[]>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Get list of past dines
     * -----------------------------------------------------------------------
     * @param key
     */
    public myFriends(key: AccessKey) {
        
        const url = this.target + "/api/v1/me/friends";
        
        return this.client.get<User[]>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Get list of past dines
     * -----------------------------------------------------------------------
     * @param key
     */
    public myPastDines(key: AccessKey) {
    
        const url = this.target + "/api/v1/me/dines/past";
    
        return this.client.get<Dine[]>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Get list of invites sent
     * -----------------------------------------------------------------------
     * @param key
     */
    public myInvitesSent(key: AccessKey) {
    
        const url = this.target + "/api/v1/me/invites/sent";
    
        return this.client.get<Invite[]>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
    
        
    }
    
    /**
     * Get list of incoming invites
     * -----------------------------------------------------------------------
     * @param key
     */
    public myInvitesReceived(key: AccessKey) {
    
        const url = this.target + "/api/v1/me/invites/received";
    
        return this.client.get<Invite[]>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
    
        
    }
    
    /**
     * Accept some invite
     * -----------------------------------------------------------------------
     * @param key
     * @param id
     */
    public myInviteAccept(key: AccessKey, id: number) {
    
        const url = this.target + "/api/v1/me/invite/" + id +  "/accept";
    
        return this.client.get<Invite>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Make some invite tentative
     * -----------------------------------------------------------------------
     * @param key
     * @param id
     */
    public myInviteTentative(key: AccessKey, id: number) {
    
        const url = this.target + "/api/v1/me/invite/" + id +  "/tentative";
    
        return this.client.get<Invite>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Reject some Invite
     * -----------------------------------------------------------------------
     * @param key
     * @param id
     */
    public myInviteReject(key: AccessKey, id: number) {
    
        const url = this.target + "/api/v1/me/invite/" + id +  "/reject";
    
        return this.client.get<Invite>(url,{
            headers: {
                "Authorization":"Bearer " + key.key
            }
        });
        
    }
    
    /**
     * Get list of dictators
     * -----------------------------------------------------------------------
     */
    public getDictators() {
    
        const url = this.target + "/api/v1/dictators";
    
        return this.client.get<User[]>(url);
        
    }
    
}

export interface User {
    id: number;
    email: string;
    name: string;
    first: string;
    last: string;
    dinesGathered: number;
    dinesBeen: number;
    accessKeys?: AccessKey[];
}

export interface AccessKey {
    key: string;
    createdAt: string;
    expiresAt: number;
}

export interface Dine {
    id?: number;
    title: string;
    message: string;
    status?: DineStatus;
    total?: number;
    server?: User;
    host?: User;
    guests?: User[];
    place?: Place;
    invite?: Invite[];
}

export interface Place {
    id: number;
    name: string;
    rating: number;
    visits: number;
    address: string;
    createdAt: string;
    dines?: Dine[];
    invites?: Invite[];
}

export interface Invite {
    id: number;
    status: InvitationStatus;
    host: User;
    invitee: User;
    place: Place;
    dine: Dine;
}

export enum DineStatus {
    PENDING,
    PROCESSING,
    CLOSED
}

export enum InvitationStatus {
    PENDING,
    ACCEPTED,
    TENTATIVE,
    REJECTED,
    CLOSED,
    EXPIRED
}
