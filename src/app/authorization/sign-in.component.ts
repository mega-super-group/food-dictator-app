import {Component, OnInit} from '@angular/core';
import {AuthService, FacebookLoginProvider} from 'angular-6-social-login';
import {ApiService} from '../service/api.service';
import {App} from '../app';
import {Router} from '@angular/router';

@Component({
    selector: 'app-sign-in',
    template: `
        
        <div class="container">

            <div class="row">
                
                <div class="col-md-4"> </div>
	            <div class="col-md-4 welcome-offset text-center">
              
		            <h2 class="text-center"> Welcome </h2>

                    <span *ngIf="!enableAuthorization">
                        {{checkingAuthorizationMsg}}
                    </span>
                    
		            <button class="btn btn-primary btn-lg"
                            *ngIf="enableAuthorization"
                            (click)="socialSignIn('facebook')">
                        Sign in with Facebook
                    </button>
                
                </div>
	            <div class="col-md-4"> </div>
             
            </div>
            
         
        </div>
        
  `,
    styles: [`
        .welcome-offset {padding-top: 100px}
  `]
})

export class SignInComponent implements OnInit {
    
    public enableAuthorization = false;
    
    public checkingAuthorizationMsg = "One second, checking if you already authorized";
    
    public errorMessage = null;
    
    constructor( readonly socialAuthService: AuthService,
                 readonly apiService: ApiService,
                 readonly router: Router) {
    
    }
    
    public socialSignIn(socialPlatform: string) {
    
        this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
            
            (userData) => {
                
                console.log(socialPlatform+" sign in data : " , userData);
                
                this.authorizeFacebookKey(userData.token);
                
            }
            
        );
        
    }
    
    public authorizeFacebookKey(key: string) {
        
        this.apiService.createOrGetUserUsingFacebookKey(key).subscribe(user => {
            
            App.setUser(user);
            App.setAuthorized();
            this.router.navigate(["places"]);
            
            
        }, err => {
            
            this.errorMessage = "We cannot authorize you, because our backend probably dead";
            
        });
        
    }
    
    public checkApiAuthorization() {
    
        if(App.tryLoadUser()) {
            
            this.apiService.checkAuthorization(App.getUser().accessKeys[0]).subscribe(a => {
    
                this.router.navigate(["places"]);
                
            }, err => {
                
                this.enableAuthorization = true;
                
            });
            
        } else {
            
            this.enableAuthorization = true;
            
        }
        
    }
    
    ngOnInit(): void {
        
        this.checkApiAuthorization();
    }
    
}
