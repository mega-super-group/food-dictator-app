# start with the node:alpine image
FROM node:alpine

# setup dependencies and copy app as root
RUN apk update && apk upgrade && apk add git
RUN apk --no-cache add g++ gcc libgcc libstdc++ linux-headers make python
RUN npm install --quiet node-gyp -g

WORKDIR /service
RUN npm install typescript -g
COPY package.json .
RUN npm install
COPY . .
EXPOSE 4200/tcp
CMD ["npm", "run", "start-dckr"]
